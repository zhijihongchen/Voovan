package org.voovan.http.client;

public enum HttpClientStatus {
	WORKING,IDLE,PREPARE,CLOSED
}
